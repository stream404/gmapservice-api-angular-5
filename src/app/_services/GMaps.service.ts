import {Injectable, NgZone} from '@angular/core';
import {GoogleMapsAPIWrapper, MapsAPILoader} from '@agm/core';
import {Observable} from 'rxjs/Observable';

declare var google: any;

@Injectable()
export class GMapsService extends GoogleMapsAPIWrapper {
  constructor(private __loader: MapsAPILoader, private __zone: NgZone) {
    super(__loader, __zone);
  }

  getLatLan( address: string) {
    console.log('Getting Address - ', address);
    const geocoder = new google.maps.Geocoder();
    return Observable.create(observer => {
      geocoder.geocode({'address': address}, function (results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
          observer.next(results[0]);
          observer.complete();
        } else {
          console.log('Error - ', results, ' & Status - ', status);
          observer.next({});
          observer.complete();
        }
      });
    });
  }

  getDistance( origin, destination) {
    const directionsService = new google.maps.DirectionsService;
    console.log('Getting route - ', origin, destination);

    return Observable.create(observer => {
      directionsService.route({
        origin: {lat: origin.lat, lng: origin.lng},
        destination: {lat: destination.lat, lng: destination.lng},
        waypoints: [],
        optimizeWaypoints: true,
        travelMode: 'DRIVING'
      }, function(response, status) {
        if (status === 'OK') {
          // directionsDisplay.setDirections(response);
          observer.next(response);
          observer.complete();
        } else {
          console.log('Directions request failed due to ' + status + ' //' + response );
          window.alert('Cannot get route');

          observer.next({});
          observer.complete();
        }
      });

    });
  }



}
