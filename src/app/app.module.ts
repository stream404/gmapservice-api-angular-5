import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {FormsModule} from '@angular/forms';

import { GmapComponent } from './gmap/gmap.component';
import {AgmCoreModule} from '@agm/core';
import {GMapsService} from './_services/GMaps.service';

@NgModule({
  declarations: [
    AppComponent,
    GmapComponent
  ],
  imports: [
    FormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyC0r73aOtyocAd579xx2Zdlrs8cKyVMp5E'
      // apiKey: 'AIzaSyA4TJKt7d5FFAqrXVUYOQLiRfyIu46gTtU'
      // apiKey: 'AIzaSyC3m2q6nraPUnoeiiTVhTr2q5YMzfBjBT4'
    }),
    BrowserModule
  ],
  providers: [GMapsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
