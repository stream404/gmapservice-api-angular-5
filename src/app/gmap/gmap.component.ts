import {Component, OnInit} from '@angular/core';
import {GMapsService} from '../_services/GMaps.service';

@Component({
  selector: 'app-gmap',
  templateUrl: './gmap.component.html',
  styleUrls: ['./gmap.component.css']
})
export class GmapComponent implements OnInit {

  public addresString = 'Wieliczka piłsudskiego'
  public addresString2 = 'Piłsudskiego kraków'
  public adresLine1
  public adresLine2
  public orgin = {'lat': 0, 'lng': 0}
  public destination = {'lat': 0, 'lng': 0}
  public route = []
  constructor(public gmapService: GMapsService) {
  }


  getCoordinates() {



    const adr1 = this.gmapService.getLatLan(this.addresString).subscribe(coor1 => {

      if (coor1) {

        this.adresLine1 = coor1.formatted_address;
        this.orgin.lat = coor1.geometry.location.lat();
        this.orgin.lng = coor1.geometry.location.lng();

        if (this.addresString2 !== '') {
          const addr2 = this.gmapService.getLatLan(this.addresString2).subscribe(cool2 => {


            if (cool2) {
              this.adresLine2 = cool2.formatted_address;
              this.destination.lat = cool2.geometry.location.lat();
              this.destination.lng = cool2.geometry.location.lng();

                const destin = this.gmapService.getDistance(this.orgin, this.destination).subscribe(troute => {

                  if (troute) {
console.log(troute);

                    troute.routes.forEach( ro => {
                        const routeObj = {'time': troute.routes[0].legs[0].duration.text,
                                          'distance': troute.routes[0].legs[0].distance.text};
                        this.route.push(routeObj);
                    });
                  }

                });

            }


          });
        }

      }
    });
  }

  ngOnInit() {


  }

}
